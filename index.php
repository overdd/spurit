<?php
$dbconn = new PDO("pgsql:dbname='postgres';
                host='localhost'",
                'postgres','admin');
include 'func.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>My To-Do List</title>
    <link rel="stylesheet" href="style.css" type="text/css"/>

</head>
<body>
  <div align=center>
        <h2>My personal fancy To-Do list</h2> <br>
        <input type="button" class="addtaskbutton" name="unhider" value="Add some task"><br><br>
  </div>
  <div class = "status" id="status"></div>
  <div id="content-wrap">
    <div id="content">
      <div class="hiddeninputfields" id="hiddeninputfields" style="display:none;">
      <table class="addtasktable">
       <tr>
        <td>Task name here</td>
        <td>Task comment here</td>
        <td>Task status here</td>
      </tr>
      <tr>
        <td><input class="text" type="text" name="article_title_new" placeholder="Name of the task..."></input></td>
        <td><input class="text" type="text" name="article_description_new"  placeholder="Some description here..."></input></td>
        <td><select size="4" class="text" name="article_status_new">
                <option disabled>Choose the status</option>
                <option value="1">TODO</option>
                <option value="2">DOING</option>
                <option value="3">DONE</option>
             </select></td>
      </tr>
      <tr>
        <td colspan=3><input type="button" class="addtaskbutton" name="addArticle" value="OK"></td>
      </tr>
      </table>
      </div>

<br>
       <div style="display:none;" id="hiddencommentfields">
       <table class="addtasktable">
            <tr>
                  <td>Write your comment</td>
                  <td><input type='text' name="newcommenttext" autofocus></input></td>
                  <td><input name="addComment" type=button value="OKay"></input></td>
            </tr>
        </table>
        </div>

      <table class="maintable" border="0" cellspacing="0" cellpadding="0">
        <caption>
        To-do List by Category
        </caption>
        <tr>
          <th>TODO</th>
          <th>DOING</th>
          <th>DONE</th>
        </tr>
        <?php
          $sql = 'Select * FROM spurit';
          $result = $dbconn->query($sql);

          $colResult = $result->rowCount();
           //echo($colResult.'<br>'); //определяем количество полученных записей
          if($colResult > 0)
          {
            foreach ($dbconn->query($sql) as $row)
            {
               echo"<tr>";
               switch ($row["status"]) {
                   case 1:
                       renderTodo($row);
                       break;
                   case 2:
                       renderDoing($row);
                       break;
                   case 3:
                       renderDone($row);
                       break;
                   default:
                   echo "Error!";
               };

               echo "</tr>";
            }
          }
        ?>
      </table>

    </div>

    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/jquery-ui-1.12.1.custom/jquery-ui.js"></script>
    <script src="js/ejs.js"></script>
    <script src="js/main.js"></script>
</body>
</html>