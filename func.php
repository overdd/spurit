<?php

function isJson($string) {
    json_decode($string);
 return (json_last_error() == JSON_ERROR_NONE);
 }

function renderTodo($row) {
$rowId=$row['id'];
$rowDescription=$row['description'];
$dateOfCreation=$row['dateofcreation'];
$lastModified=$row['lastmodified'];

if (isJson($row['commentjson'])) {
    $numberOfComments=count(json_decode($row['commentjson'], true));
}
else {
    $numberOfComments="no";
};
    print<<<HERE
       <td>This is task No. <b>$rowId</b><br>
       Current status is <b>TODO</b>.  Switch to <a href="editstatus.php?id=$rowId&status=1">[Doing]</a><br>
       Description: <b>$rowDescription</b><br>
       Task created: <b>$dateOfCreation</b><br>
       Task last modified: <b>$lastModified</b><hr>
       This task has <b>$numberOfComments</b> comments<br>
       <input type=button name="unhidercomments" value="Add new comment"></input><br>
       </td>
    HERE;
    echo "<td></td>";
    echo "<td> </td>";

//<center><input type='checkbox' name='delete_button[]' value='".$row['id']."'></center>
}

function renderDoing($row) {
$rowId=$row['id'];
$rowDescription=$row['description'];
$dateOfCreation=$row['dateofcreation'];
$lastModified=$row['lastmodified'];
$arr = json_decode($row['commentjson'], true);
if (isJson($row['commentjson'])) {
    $numberOfComments=count(json_decode($row['commentjson'], true));
    $jsonfordump = json_decode($row['commentjson'], true);
}
else {
    $numberOfComments="no";
};
    echo "<td></td>";
    print<<<HERE
       <td>This is task No. <b>$rowId</b><br>
       Current status is <b>TODO</b>. Switch to <a href="editstatus.php?id=$rowId&status=2">[Done]</a><br>
       Description: <b>$rowDescription</b><br>
       Task created: <b>$dateOfCreation</b><br>
       Task last modified: <b>$lastModified</b><hr>
       This task has <b>$numberOfComments</b> comments<br>
       </td>

    HERE;
    echo "<td> </td>";
//<center><input type='checkbox' name='delete_button[]' value='".$row['id']."'></center>
}

function renderDone($row) {
$rowId=$row['id'];
$rowDescription=$row['description'];
$dateOfCreation=$row['dateofcreation'];
$lastModified=$row['lastmodified'];
if (isJson($row['commentjson'])) {
    $numberOfComments=count(json_decode($row['commentjson'], true));
}
else {
    $numberOfComments="no";
};
    echo "<td></td>";
    echo "<td></td>";
    print<<<HERE
       <td>This is task No. <b>$rowId</b><br>
        Current status is <b>TODO</b><br>
       Description: <b>$rowDescription</b><br>
       Task created: <b>$dateOfCreation</b><br>
       Task last modified: <b>$lastModified</b><hr>
       This task has <b>$numberOfComments</b> comments<br>
        </td>

    HERE;
//<center><input type='checkbox' name='delete_button[]' value='".$row['id']."'></center>
}


?>